/**************************************************************
*
*	CtrlPanel Test
*
***************************************************************/

#include "./libBBB/libBBB.h"
#include "math.h"
#include "PID.h"

/* LIBRARIES NEEDED FOR LCM*/
#include <inttypes.h>
#include <lcm/lcm.h>
#include "lcmtypes/ctrlPanelData_t.h"  // ctrlPanel lcm type
#include "lcmtypes/pidData_t.h"  // PID lcm type

//Sensor Limit calibration
//MEASURE FOR YOUR PANEL
#define F1MIN 42
#define F1MAX 1736
#define F2MIN 42
#define F2MAX 1737
#define P1MIN 2
#define P1MAX 1767
#define P2MIN 1
#define P2MAX 1767
#define P3MIN 7
#define P3MAX 1767

//set range of pot control
//YOU NEED TO DEFINE THESE
#define KPMAX 0
#define KIMAX 0
#define KDMAX 0

struct Ctrlpanel_data{
  	int fader1;
  	int fader2;
  	int pots[3];
	int switches[3];
    int motorSignal;
	float fader1_scaled;
	float fader2_scaled;
	float pots_scaled[3];
};

void InitializePanel(void){

 	/* Initiate Pin 66 Linked to Switch 1 */
    //INCLUDE OTHER INPUTS!
        initPin(66);
        setPinDirection(66, INPUT);
        initPin(67);
        setPinDirection(67, INPUT);
        initPin(69);
        setPinDirection(69, INPUT);

        /* Initiate Pin 45 that defines motor direction */
        initPin(45);
        setPinDirection(45, OUTPUT);
	
        /* Initiate A/D Converter */
        initADC();

        /* Initiate PWM for Servo 1 */
        initPWM(SV1);
        setPWMPeriod(SV1H, SV1, 100000);
        setPWMPolarity(SV1H, SV1, 0);
        setPWMDuty(SV1H, SV1, 0);
        setPWMOnOff(SV1H, SV1, ON);

}

/*******************************************    
*       Scaling functions to scale AD readings
*       0 to 1 for pots and -1 to 1 for faders
*******************************************/

float ScaleInt2PosFloat(int input, int min, int max){
    //scale readings to between 0 and 1
    //IMPLEMENT ME!
    return ((float)(input - min))/((float)(max - min));
}

float ScaleInt2Float(int input, int min, int max){
    //scale readings to between -1 and 1
    //IMPLEMENT ME!
    return (2*(float)(input - min))/((float)(max - min)) - 1.0;
}

/*******************************************    
*       Read values of the sensors
*       IMPLEMENT THIS FIRST TO TEST 
*******************************************/

void ReadPanelValues(struct Ctrlpanel_data * ctrlpanel){

	/* Read ADCs and give time after each reading to settle */
	ctrlpanel->fader1 = readADC(HELPNUM, A0); // WHAT IS HELPNUM?
	usleep(100);
	//ctrlpanel->fader2 = readADC(HELPNUM, A1);
	ctrlpanel->fader2 = 0.5*(F2MAX-F2MIN) + 0.6*0.5*(F2MAX-F2MIN)*(sin(2*3.14159*11*utime_now()/1000000));
	usleep(100);
	ctrlpanel->pots[0] = readADC(HELPNUM, A2);
	usleep(100);
    //ADD OTHER POTENTIOMETERS
	ctrlpanel->pots[1] = readADC(HELPNUM, A3);
	usleep(100);
	ctrlpanel->pots[2] = readADC(HELPNUM, A4);
	usleep(100);
	/* Scale the readings */
    //UNCOMMENT WHEN READY TO IMPLEMENT
	ctrlpanel->fader1_scaled = -ScaleInt2Float(ctrlpanel->fader1,F1MIN, F1MAX);	
	ctrlpanel->fader2_scaled = -ScaleInt2Float(ctrlpanel->fader2,F2MIN, F2MAX);	// need to change to F2MIN?
	//ctrlpanel->fader2_scaled = 0.4*sin(2*3.14159*10*utime_now()/1000000);
	//int i = 0;
	//for(i=0;i<3;i++)
	ctrlpanel->pots_scaled[0] = ScaleInt2PosFloat(ctrlpanel->pots[0],P1MIN, P1MAX);
	ctrlpanel->pots_scaled[1] = ScaleInt2PosFloat(ctrlpanel->pots[1],P2MIN, P2MAX);
	ctrlpanel->pots_scaled[2] = ScaleInt2PosFloat(ctrlpanel->pots[2],P3MIN, P3MAX);
    

	/* Read Switches position */
	ctrlpanel->switches[0] = getPinValue(66);
    //ADD ADDITIONAL SWITCHES 
    ctrlpanel->switches[1] = getPinValue(67);
    ctrlpanel->switches[2] = getPinValue(69);
}


void PrintPanelValues(struct Ctrlpanel_data * ctrlpanel){

	// CHANGE TO PRINT ALL INPUTS AND EVENTUAL OUTPUTS YOU ALSO WANT TO CHECK
	printf("fader1: %4d\n",	ctrlpanel->fader1);
	printf("fader2: %4d\n",	ctrlpanel->fader2);
	//printf("pot1: %4d\n",	ctrlpanel->pots[0]);
	//printf("pot2: %4d\n",	ctrlpanel->pots[1]);
	//printf("pot3: %4d\n",	ctrlpanel->pots[2]);
	
	//printf("fader1_s: %4f\n",	ctrlpanel->fader1_scaled);
	//printf("fader2_s: %4f\n",	ctrlpanel->fader2_scaled);
	//printf("pot1_s: %4f\n",	ctrlpanel->pots_scaled[0]);
	//printf("pot2_s: %4f\n",	ctrlpanel->pots_scaled[1]);
	//printf("pot3_s: %4f\n",	ctrlpanel->pots_scaled[2]);
	
	
	
	printf("switch1: %4d\n",	ctrlpanel->switches[0]);
	printf("switch2: %4d\n",	ctrlpanel->switches[1]);
	printf("switch3: %4d\n",	ctrlpanel->switches[2]);

	printf("motorSignal: %d\n",	ctrlpanel->motorSignal); //SV signals?
	printf("motor direction: %d\n",	getPinValue(45));
}

/*******************************************    
*       
*       CONTROLLERS
*
*******************************************/
void SimpleFaderDriver(struct Ctrlpanel_data * ctrlpanel){
	
	/* Drive Fader1 motor with Fader 2 position */
	/* Position of Fader 2 is reflected in the speed of Fader 1 */
	/* Fader 2 in middle = Fader 1 does not move */
	/* Fader 2 left = Fader 1 moves to left up to limit */
	/* Fader 2 right = Fader 1 moves to right up to limit */
	  
	ctrlpanel->motorSignal = 200*(ctrlpanel->fader2-1700/2);
      	setPinValue(45, ctrlpanel->motorSignal > 0);
      	setPWMDuty(SV1H, SV1, abs(ctrlpanel->motorSignal));

}

void BangBang(struct Ctrlpanel_data * ctrlpanel){
    //IMPLEMENT ME!
    //float errorspan = F2MAX - F1MIN;
    float error = (float)(ctrlpanel->fader2 - ctrlpanel->fader1);
    //ctrlpanel->motorSignal = abs(error) < 100 ? 0 : error; // 
    printf("error: %4f\n",	error);
    //printf("motorSignal: %4d\n",	ctrlpanel->motorSignal);
    setPinValue(45, error > 0);
    setPWMDuty(SV1H, SV1, (abs(error) < 50 ? 0 : 100000));
}

void PID(PID_t *pid, struct Ctrlpanel_data * ctrlpanel){
    //IMPLEMENT ME!
    /*
    float errorspan = F2MAX - F1MIN;
    float error = 100000*(float)(ctrlpanel->fader2 - ctrlpanel->fader1) / errorspan;
    ctrlpanel->motorSignal = abs(error) < 1 ? 0 : error; // 
    printf("error: %4f\n",	error);
    printf("motorSignal: %4d\n",	ctrlpanel->motorSignal);
    setPinValue(45, ctrlpanel->motorSignal > 0);
    setPWMDuty(SV1H, SV1, abs(ctrlpanel->motorSignal));
	*/
	float kp = 750000*ctrlpanel->pots_scaled[0];
	float ki = 2000*ctrlpanel->pots_scaled[1];
	float kd = 1000000*ctrlpanel->pots_scaled[2];
	//printf("kp: %4f\n",kp);
	//printf("ki: %4f\n",ki);
	//printf("kd: %4f\n",kd);
	PID_SetTunings(pid,(ctrlpanel->switches[0]==0?kp:0),(ctrlpanel->switches[1]==0?ki:0),(ctrlpanel->switches[2]==0?kd:0));
	pid->pidSetpoint = ctrlpanel->fader2_scaled;
	pid->pidInput = pid->pidSetpoint - ctrlpanel->fader1_scaled;
	printf("pidInput: %4f\n",pid->pidInput);
	PID_Compute(pid);
	ctrlpanel->motorSignal = pid->pidOutput;
	printf("pidOutput: %4f\n",pid->pidOutput);
	
	setPinValue(45, ctrlpanel->motorSignal < 0);
    setPWMDuty(SV1H, SV1, abs(ctrlpanel->motorSignal));
	
}


/*******************************************    
*       
*       LCM Message Preparation
*
*******************************************/

void CtrlPanelDataLCM(struct Ctrlpanel_data * ctrlpanel, const char *channel, lcm_t * lcm){

        ctrlPanelData_t msg = {};
        
	    /* Prepare message to send over lcm channel*/
        msg.timestamp = utime_now();
        msg.fader1 = ctrlpanel->fader1;
        msg.fader2 = ctrlpanel->fader2;
        msg.pots[0] = ctrlpanel->pots[0];
        msg.pots[1] = ctrlpanel->pots[1];
        msg.pots[2] = ctrlpanel->pots[2];
        msg.switches[0] = ctrlpanel->switches[0];
        msg.switches[1] = ctrlpanel->switches[1];
        msg.switches[2] = ctrlpanel->switches[2];

        ctrlPanelData_t_publish(lcm, channel, &msg);
}

void PIDLCM(struct Ctrlpanel_data * ctrlpanel, PID_t *pid, const char *channel, lcm_t * lcm){
	
	pidData_t msg = {};
	
    
    //IMPLEMENT ME!
    msg.timestamp = utime_now();
    msg.pidInput = pid->pidInput;
    msg.pidSetpoint = pid->pidSetpoint;
    msg.pidOutput = pid->pidOutput;
    msg.Kp = pid->kp;
    msg.Ki = pid->ki;
    msg.Kd = pid->kd;

    pidData_t_publish(lcm, channel, &msg);
}



/*******************************************    
*       
*       MAIN FUNCTION
* 
*******************************************/
int main()
{

	struct Ctrlpanel_data ctrlpanel;
        
	// For Controllers
	int64_t thisTime, lastTime, timeDiff;
	int updateRate = 1000;

	// PID
	PID_t *pid;
	pid = PID_Init(0,0,0);
	PID_SetUpdateRate(pid, updateRate);

        // LCM
        const char *channel1 = "CTRL_PANEL_DATA";   // LCM channel to publish messages
        const char *channel2 = "PIDDATA";   // LCM channel to publish messages
        lcm_t * lcm = lcm_create(NULL);
        if(!lcm) {
                return 1;
                }

	// Panel Initialization
	InitializePanel();

	/* Never ending loop */	
	while(1)
	{
		

		// Control at a specified frequency
		thisTime = utime_now();
                timeDiff = thisTime - lastTime;
                if(timeDiff >= updateRate){
			ReadPanelValues(&ctrlpanel);
			lastTime = thisTime;
			//SimpleFaderDriver(&ctrlpanel);
			//BangBang(&ctrlpanel);
			PID(pid,&ctrlpanel);
			CtrlPanelDataLCM(&ctrlpanel,channel1,lcm);	
			//PIDLCM(&ctrlpanel,pid,channel2,lcm);
		}

		//PrintPanelValues(&ctrlpanel);
		
		// UNCOMMENT TO INCLUDE LCM MESSAGE
			
	
	}

	return 0;
}


