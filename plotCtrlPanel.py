import lcm
import numpy as np
import matplotlib.pyplot as plt
import time
import math
import csv
import sys
sys.path.append('./lcmtypes')
from ctrlpanel import ctrlPanelData_t

f1 = []
f2 = []
t = []
plt.ion()
fig = plt.figure()
plt.title('Control Panel')


def plot_handler(channel, data):
    msg = ctrlPanelData_t.decode(data)
    f1.append(msg.fader1)
    f2.append(msg.fader2)
    t.append(msg.timestamp)
    fig.canvas.draw()

lc = lcm.LCM()
subscription = lc.subscribe("CTRL_PANEL_DATA", plot_handler)

try:
    while True:
        lc.handle()
        plt.plot(t, f1, 'r')
        plt.plot(t, f2, 'b')

except KeyboardInterrupt:
    np.savetxt("Time.csv",t,delimiter=' ')
    np.savetxt("f1.csv",f1,delimiter=' ')
    np.savetxt("f2.csv",f2,delimiter=' ')
    print t
    print f1
    

lc.unsubscribe(subscription)
  
